from selenium import webdriver
from os.path import dirname
import os
from selenium.webdriver.chrome.options import Options

dirpath = os.path.abspath(os.path.dirname(__file__))
dirname(dirpath)
chromedriver_path = (dirname(dirpath)) + "/chromedriver"

print(chromedriver_path)

class Browser(object):
    chrome_options = Options()
    chrome_options.add_argument("--disable-infobars") #disabling infobars
    chrome_options.add_argument("--disable-extensions") # disabling extensions
    chrome_options.add_argument("--disable-gpu") # applicable to windows os only
    chrome_options.add_argument("--disable-dev-shm-usage") #// overcome limited resource problems
    chrome_options.add_argument("--no-sandbox") #// Bypass OS security model
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=chrome_options)
    driver.implicitly_wait(30)
    driver.set_page_load_timeout(30)
    driver.maximize_window()

    
    

    def close(context):
        context.driver.quit()
